﻿using UnityEngine;
using System.Collections;

public class UIButton : UIThing {
	
	public enum eConstraint {
		FREE,
		HORIZONTAL,
		VERTICAL
	}
	public eConstraint constraint;
	
	public override bool MouseDown(Vector3 pos) {
		clickPos = pos - transform.localPosition;
		if(container) {
			container.MouseDown(pos);
		}
		StartCoroutine(Bounce());
		return true;
	}
	
	public override bool MouseUp(Vector3 pos) {
		StartCoroutine(Bounce());
		return true;
	}
	
	public override bool Drag(Vector3 pos) {
		if(container) {
			container.Drag(pos);
			return true;
		}
		
		Vector3 delta = pos - clickPos;
		delta.z = 0;
		if(constraint == eConstraint.HORIZONTAL) {
			delta.y = 0;
		}
		if(constraint == eConstraint.VERTICAL) {
			delta.x = 0;
		}
		transform.localPosition = delta;
		return true;
	}
	
	IEnumerator Bounce() {
		float t = 0;
		float duration = 0.2f;
		Vector3 start = Vector3.one;
		Vector3 end = start * 1.2f;
		while(t < 1) {
			t += Time.deltaTime / duration;
			transform.localScale = Vector3.Lerp(start, end, Mathf.Sin(t * Mathf.PI));
			yield return null;
		}
		transform.localScale = start;
	}
}
