﻿using UnityEngine;
using System.Collections;

public class InputManager : Singleton<InputManager> {
	
	private UIThing activeUiThing;
	private bool isDragging;
	
	//public void Tick(float dt) {
	void Update() {
		if(Input.GetMouseButtonDown(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)) {
				UIThing uiThing = hit.collider.GetComponent<UIThing>();
				if(uiThing) {
					isDragging = uiThing.isDraggable;
					uiThing.MouseDown(hit.point);
					activeUiThing = uiThing;
				}
			}
		}
		if(Input.GetMouseButtonUp(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)) {
				UIThing uiThing = hit.collider.GetComponent<UIThing>();
				if(uiThing && activeUiThing == uiThing) {
					uiThing.MouseUp(hit.point);
				}
			}
			isDragging = false;
		}
		if(isDragging) {
			activeUiThing.Drag(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		}
	}
	
}
