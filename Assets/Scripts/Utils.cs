﻿using UnityEngine;
using System.Collections;

public static class Utils {

	public static float Luminance(this Color c) {
		return c.r*0.299f + c.g*0.587f + c.b*0.114f;
	}
	
	public static float Luminance(this Color32 c) {
		return c.r*0.299f + c.g*0.587f + c.b*0.114f;
	}
	
	public static int HexToInt(char hexChar) {
		switch(System.Char.ToUpper(hexChar)) {
			case '0': return 0;
			case '1': return 1;
			case '2': return 2;
			case '3': return 3;
			case '4': return 4;
			case '5': return 5;
			case '6': return 6;
			case '7': return 7;
			case '8': return 8;
			case '9': return 9;
			case 'A': return 10;
			case 'B': return 11;
			case 'C': return 12;
			case 'D': return 13;
			case 'E': return 14;
			case 'F': return 15;
 		}
		return -1;
	}

	public static Color HexToColor(string hex) {
		hex = hex.Replace("#", "");
		if(hex.Length < 6 && hex.Length != 8) {
			Debug.LogError("Hex input string should be in the format '#XXXXXXXX' (6 or 8 digits)");
			return new Color(1, 0, 1, 1);	//magenta
		}
		float r = Utils.HexToInt(hex[0]) * 16 + Utils.HexToInt(hex[1]);
		float g = Utils.HexToInt(hex[2]) * 16 + Utils.HexToInt(hex[3]);
		float b = Utils.HexToInt(hex[4]) * 16 + Utils.HexToInt(hex[5]);
		float a;
		if(hex.Length == 8) {
			a = Utils.HexToInt(hex[6]) * 16 + Utils.HexToInt(hex[7]);
		}
		else {
			a = 255f;
		}
		r /= 255f;
		g /= 255f;
		b /= 255f;
		a /= 255f;
		return new Color(r,g,b,a);
	}
	
	public static float GetHue(Color c) {
		float min, max, delta;
		min = Mathf.Min(c.r, Mathf.Min(c.g, c.b));
		max = Mathf.Max(c.r, Mathf.Max(c.g, c.b));
		delta = max - min;
		if(delta == 0) {
			return 0;
		}
		
		float h;
		if(c.r == max) {
			h = 60 * ((c.g - c.b) / delta);		// between yellow & magenta
		}
		else if(c.g == max) {
			h = 60 * (2 + (c.b - c.r) / delta);	// between cyan & yellow
		}
		else {
			h = 60 * (4 + (c.r - c.g) / delta);
		}
		if(h < 0) {
			h += 360;
		}
		return h;
	}
	
	public static float GetSaturation(Color c) {
		float min, max, delta;
		min = Mathf.Min(c.r, Mathf.Min(c.g, c.b));
		max = Mathf.Max(c.r, Mathf.Max(c.g, c.b));
		delta = max - min;
		return max == 0 ? 0 : delta / max;
	}
	
	public static float GetValue(Color c) {
		return Mathf.Max(c.r, Mathf.Max(c.g, c.b));
	}
	
	public static Color HSVtoRGB(float h, float s, float v, float a = 1) {
		if(s == 0) {
			return new Color(v, v, v, a);
		}
		h /= 60;
		int i = Mathf.FloorToInt(h);
		Color col = new Color();
		float f = h - i;
		float p = v * (1 - s);
		float q = v * ( 1 - s * f );
		float t = v * ( 1 - s * ( 1 - f ) );
		switch(i) {
		case 0:
			col.r = v;
			col.g = t;
			col.b = p;
			break;
		case 1:
			col.r = q;
			col.g = v;
			col.b = p;
			break;
		case 2:
			col.r = p;
			col.g = v;
			col.b = t;
			break;
		case 3:
			col.r = p;
			col.g = q;
			col.b = v;
			break;
		case 4:
			col.r = t;
			col.g = p;
			col.b = v;
			break;
		default:		// case 5:
			col.r = v;
			col.g = p;
			col.b = q;
			break;
		}
		col.a = a;
		return col;
	}
	/*
void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v )
{
	int i;
	float f, p, q, t;
	if( s == 0 ) {
		// achromatic (grey)
		*r = *g = *b = v;
		return;
	}
	h /= 60;			// sector 0 to 5
	i = floor( h );
	f = h - i;			// factorial part of h
	p = v * ( 1 - s );
	q = v * ( 1 - s * f );
	t = v * ( 1 - s * ( 1 - f ) );
	switch( i ) {
		case 0:
			*r = v;
			*g = t;
			*b = p;
			break;
		case 1:
			*r = q;
			*g = v;
			*b = p;
			break;
		case 2:
			*r = p;
			*g = v;
			*b = t;
			break;
		case 3:
			*r = p;
			*g = q;
			*b = v;
			break;
		case 4:
			*r = t;
			*g = p;
			*b = v;
			break;
		default:		// case 5:
			*r = v;
			*g = p;
			*b = q;
			break;
	}
}	*/
}
