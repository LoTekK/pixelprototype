﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorGui : MonoBehaviour {
	
	public Color col;
	public RectTransform HSVRect;
	public RectTransform RGBRect;
	public Slider hueSlider;
	public Slider saturationSlider;
	public Slider valueSlider;
	public Slider rSlider;
	public Slider gSlider;
	public Slider bSlider;
	public Image thumb;
	public PixelEdit pixelEdit;
	private float _hue, _saturation, _value;
	public float hue {
		get {
			return _hue;
		}
		set {
			_hue = value;
			UpdateColor();
		}
	}
	public float saturation {
		get {
			return _saturation;
		}
		set {
			_saturation = value;
			UpdateColor();
		}
	}
	public float value {
		get {
			return _value;
		}
		set {
			_value = value;
			UpdateColor();
		}
	}
	public float r {
		get {
			return col.r;
		}
		set {
			col.r = value;
			UpdateHSV();
		}
	}
	public float g {
		get {
			return col.g;
		}
		set {
			col.g = value;
			UpdateHSV();
		}
	}
	public float b {
		get {
			return col.b;
		}
		set {
			col.b = value;
			UpdateHSV();
		}
	}
	
	public void ToggleMode() {
		HSVRect.gameObject.SetActive(!HSVRect.gameObject.activeSelf);
		RGBRect.gameObject.SetActive(!RGBRect.gameObject.activeSelf);
	}
	
	public void UpdateHSV() {
		_hue = Utils.GetHue(col);
		_saturation = Utils.GetSaturation(col);
		_value = Utils.GetValue(col);
		hueSlider.value = hue;
		saturationSlider.value = saturation;
		valueSlider.value = value;
		thumb.color = col;
		UpdateSliders();
	}
	
	public void UpdateColor() {
		col = Utils.HSVtoRGB(hue, saturation, value);
		rSlider.value = col.r;
		gSlider.value = col.g;
		bSlider.value = col.b;
		thumb.color = col;
		UpdateSliders();
	}
	
	private void UpdateSliders() {
		//hueSlider.GetComponent<Image>().color = col;
		saturationSlider.GetComponent<Image>().color = Utils.HSVtoRGB(hue, 1, value, value);
		valueSlider.GetComponent<Image>().color = Utils.HSVtoRGB(hue, saturation, 1, value);
		rSlider.GetComponent<Image>().color = col;
		gSlider.GetComponent<Image>().color = col;
		bSlider.GetComponent<Image>().color = col;
		if(pixelEdit.palette.Length > 0) {
			pixelEdit.palette[pixelEdit.paletteIndex] = col;
			pixelEdit.paletteEntries[pixelEdit.paletteIndex].image.color = col;
			//pixelEdit.UpdatePalette(pixelEdit.paletteIndex);
			pixelEdit.UpdatePalette();
		}
	}
	/*
	private bool hsv;

	void OnGUI() {
		hsv = GUILayout.Toggle(hsv, hsv ? "HSV" : "RGB", GUILayout.Width(Screen.width/2));
		if(hsv) {
			hue = GUILayout.HorizontalSlider(hue, 0, 360);
			saturation = GUILayout.HorizontalSlider(saturation, 0, 1);
			value = GUILayout.HorizontalSlider(value, 0, 1);
			col = Utils.HSVtoRGB(hue, saturation, value);
		}
		else {
			col.r = GUILayout.HorizontalSlider(col.r, 0, 1);
			col.g = GUILayout.HorizontalSlider(col.g, 0, 1);
			col.b = GUILayout.HorizontalSlider(col.b, 0, 1);
			hue = Utils.GetHue(col);
			saturation = Utils.GetSaturation(col);
			value = Utils.GetValue(col);
		}
	}
	*/
}
