﻿using UnityEngine;
using System.Collections;

public class UIThing : MonoBehaviour {
	
	public Color c;
	[RangeAttribute(0, 360)]
	public float hue;
	[RangeAttribute(0, 1)]
	public float saturation;
	[RangeAttribute(0, 1)]
	public float value;
	public UIThing container;
	public bool _isDraggable = false;
	public bool isDraggable {
		get {
			return container ? container.isDraggable || _isDraggable : _isDraggable;
		}
	}
	protected Vector3 clickPos;
	
	public virtual bool MouseDown(Vector3 pos) {
		return true;
	}
	
	public virtual bool MouseUp(Vector3 pos) {
		return true;
	}
	
	public virtual bool Drag(Vector3 pos) {
		return true;
	}
	
	void OnValidate() {
		/*
		hue = Utils.GetHue(c);
		saturation = Utils.GetSaturation(c);
		value = Utils.GetValue(c);
		*/
		c = Utils.HSVtoRGB(hue, saturation, value);
	}
}