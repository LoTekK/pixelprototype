﻿using UnityEngine;
using System.Collections;

public static class Extensions {
	public static float Hue(this Color c) {
		float cmax = c.Value();
		float cmin = Mathf.Min(c.r, Mathf.Min(c.g, c.b));
		float delta = cmax - cmin;
		if(delta == 0) {
			return 0;
		}
		if(c.r >= c.g && c.r >= c.b) {
			return 60 * (((c.g - c.b) / delta) % 6);
		}
		if(c.g > c.r && c.g >= c.b) {
			return 60 * (((c.b - c.r) / delta) + 2);
		}
		if(c.b > c.r && c.b >= c.g) {
			return 60 * (((c.r - c.g) / delta) + 4);
		}
		return 0;
	}

	public static float Value(this Color c) {
		return Mathf.Max(c.r, Mathf.Max(c.g, c.b));
	}

	public static float Saturation(this Color c) {
		float cmax = c.Value();
		float cmin = Mathf.Min(c.r, Mathf.Min(c.g, c.b));
		float delta = cmax - cmin;
		return delta == 0 ? 0 : delta / cmax;
	}
}
