﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T:Singleton<T> {
	
	private static T s_Instance;
	public static T Instance {
		get {
			if(!Application.isPlaying) {
				return (GameObject.FindObjectOfType<T>());
			}
			if(!s_Instance) {
				Debug.LogError("Unable to find " + typeof(T) + ". Is it in the scene?");
			}
			return s_Instance;
		}
	}
	
	protected virtual void Awake() {
		if(s_Instance == null) {
			s_Instance = (T)(object)this;
		}
		else {
			Debug.LogError("More than one instance of " + typeof(T) + " found!", this);
		}
		Initialise();
	}
	
	protected virtual void Initialise() {
		
	}
}