﻿using UnityEngine;
using System.Collections;

public class UIShelf : UIThing {
	
	public enum eConstraint {
		FREE,
		HORIZONTAL,
		VERTICAL
	}
	public eConstraint constraint;
	
	public override bool MouseDown(Vector3 pos) {
		clickPos = pos - transform.localPosition;
		if(container) {
			container.MouseDown(pos);
		}
		return true;
	}
	
	public override bool Drag(Vector3 pos) {
		if(container) {
			container.Drag(pos);
			return true;
		}
		
		Vector3 delta = pos - clickPos;
		delta.z = 0;
		if(constraint == eConstraint.HORIZONTAL) {
			delta.y = 0;
		}
		if(constraint == eConstraint.VERTICAL) {
			delta.x = 0;
		}
		transform.localPosition = delta;
		return true;
	}
}
