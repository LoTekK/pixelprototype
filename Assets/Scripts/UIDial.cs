﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[SelectionBase]
[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class UIDial : UIBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {

	[SerializeField]
	private RectTransform m_Content;
	public RectTransform content { get { return m_Content; } set { m_Content = value; } }
	
	private Vector2 m_PointerStartLocalCursor = Vector2.zero;
	private Vector2 m_ContentStartPosition = Vector2.zero;
	
	private RectTransform m_ViewRect;
	private Bounds m_ContentBounds;
	private Bounds m_ViewBounds;
	private bool m_AllowHorizontal;
	private bool m_AllowVertical;
	private Vector2 m_Velocity;
	private bool m_Dragging = false;
	private Vector2 m_LastPosition = Vector2.zero;
	private float startAngle;
	private Vector3 startRotation;

	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		m_ViewRect = transform as RectTransform;
	}

	void Update() {
		int i = 0;
		foreach(RectTransform child in content) {
			child.anchoredPosition = new Vector2(Mathf.Sin(2 * Mathf.PI * i / content.childCount), Mathf.Cos(2 * Mathf.PI * i / content.childCount)) * 50;
			i++;
		}
	}
	
	public void OnBeginDrag (PointerEventData data) 
	{
		if (!IsActive ())
			return;
		
		m_PointerStartLocalCursor = Vector2.zero;
		RectTransformUtility.ScreenPointToLocalPointInRectangle (m_ViewRect, data.position, data.pressEventCamera, out m_PointerStartLocalCursor);
		m_ContentStartPosition = m_Content.anchoredPosition;
		m_Velocity = Vector2.zero;
		m_Dragging = true;
		startAngle = Vector2.Angle(Vector2.right, m_PointerStartLocalCursor) * Mathf.Sign(Vector2.Dot(-Vector2.up, m_PointerStartLocalCursor));
		startRotation = m_Content.localEulerAngles;
	}

	public void OnEndDrag(PointerEventData data)
	{
		m_Dragging = false;
	}

	public void OnDrag(PointerEventData data)
	{
		Vector2 localCursor;
		RectTransformUtility.ScreenPointToLocalPointInRectangle (m_ViewRect, data.position, data.pressEventCamera, out localCursor);
		float angle = Vector2.Angle(Vector2.right, localCursor) * Mathf.Sign(Vector2.Dot(-Vector2.up, localCursor));
		m_Content.localEulerAngles = startRotation - Vector3.forward * (angle - startAngle);
		/*
		if (!IsActive())
			return;
		
		Vector2 localCursor;
		if (!RectTransformUtility.ScreenPointToLocalPointInRectangle (m_ViewRect, data.position, data.pressEventCamera, out localCursor))
			return;
		
		UpdateBounds ();
		
		var pointerDelta = localCursor - m_PointerStartLocalCursor;
		Vector2 position = m_ContentStartPosition + pointerDelta;
		
		// Offset to get content into place in the view.
		Vector2 offset = CalculateOffset (position - m_Content.anchoredPosition);
		position += offset;
		if (m_MovementType == MovementType.Elastic)
		{
			if (offset.x != 0)
				position.x = position.x - RubberDelta (offset.x, m_ViewBounds.size.x);
			if (offset.y != 0)
				position.y = position.y - RubberDelta (offset.y, m_ViewBounds.size.y);
		}
		
		if (!m_Horizontal || !m_AllowHorizontal)
			position.x = m_Content.anchoredPosition.x;
		if (!m_Vertical || !m_AllowVertical)
			position.y = m_Content.anchoredPosition.y;
		
		m_Content.anchoredPosition = position;
		*/
	}
}
