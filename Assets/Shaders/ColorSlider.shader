Shader "Custom/ColorSlider" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		[KeywordEnum(Hue, Saturation, Value, Red, Green, Blue)] _Channel ("Channel", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		Cull Off
		Blend One OneMinusSrcAlpha
		
		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _CHANNEL_HUE _CHANNEL_SATURATION _CHANNEL_VALUE _CHANNEL_RED _CHANNEL_GREEN _CHANNEL_BLUE
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}
			
			sampler2D _MainTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord);
				half u = IN.texcoord.y;
				#if defined (_CHANNEL_HUE)
				half3 col;
				col.r = saturate(u < 0.5 ? 1-6*(u-1./6) : 6*(u-2./3));
				col.g = saturate(u < 0.5 ? 6*u : 1-6*(u-1./2));
				col.b = saturate(u < 0.5 ? 6*(u-1./3) : 1-6*(u-5./6));
				c.rgb = col;
				#elif defined (_CHANNEL_SATURATION)
				c.rgb = lerp(half3(IN.color.a,IN.color.a,IN.color.a), IN.color.rgb, u);
				#elif defined (_CHANNEL_VALUE)
				c.rgb = lerp(half3(0,0,0), IN.color.rgb, u);
				#elif defined (_CHANNEL_RED)
				c.rgb = half3(u, IN.color.g, IN.color.b);
				#elif defined (_CHANNEL_GREEN)
				c.rgb = half3(IN.color.r, u, IN.color.b);
				#elif defined (_CHANNEL_BLUE)
				c.rgb = half3(IN.color.r, IN.color.g, u);
				#endif
				c.rgb *= c.a;
				return c;
			}
		ENDCG
		}
	}
}
