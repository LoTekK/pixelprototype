﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

[System.Serializable]
public class TestData {
	public int[] testInts;
	public string name;

	public TestData(int[] ints, string name) {
		this.testInts = ints;
		this.name = name;
	}
}

public class TestSerialise : MonoBehaviour {

	// Use this for initialization
	void Start () {
		TestData t = new TestData(new int[] {0,4,5,1}, "test");
		BinaryFormatter b = new BinaryFormatter();
		FileStream f = File.Create(Application.dataPath + "/test.txt");
		b.Serialize(f, t);
		f.Close();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			BinaryFormatter b = new BinaryFormatter();
			FileStream f = File.Open(Application.dataPath + "/test.txt", FileMode.Open);
			TestData t = (TestData)b.Deserialize(f);
			Debug.Log(t.name + ": " + t.testInts[0] + ", " + t.testInts[1] + ", " + t.testInts[2] + ", " + t.testInts[3]);
		}
	}
}
