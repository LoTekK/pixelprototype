﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PixelCanvas : MonoBehaviour {

	private Camera cam;
	private Texture2D canvas;
	public Renderer quad;
	public Renderer preview;
	private int brushRadius = 1;
	private int lastX;
	private int lastY;
	public int w = 32;
	public int h = 32;
	public int paletteCount;
	public Texture2D paletteTex;
	public Color[] palette;
	private int paletteIndex = 0;
	private Texture2D colorIndicator;
	private int[] paletteIndices;
	public GUISkin skin;
	public GUISkin skin2;
	private Texture2D sliderThumb;
	private enum paintModes {
		Brush,
		Flood,
		COUNT
	}
	private paintModes paintMode;
	private float floodTolerance = 0;
	public Renderer overlay;
	public List<Texture2D> uis = new List<Texture2D>();
	private int currentUI = -1;
	private float targetZoom;
	private Vector3 dragOrigin;
	//track this instead of using canvas.GetPixels
	//potentially keep an array per palette entry? with pixel index or coord?

	// Use this for initialization
	void Start () {
		cam = Camera.main;
		palette = new Color[paletteCount];
		for(int i = 0; i < paletteCount; i++) {
			palette[i] = paletteTex.GetPixel(Mathf.FloorToInt((i+0.5f) * paletteTex.width / paletteCount), 0);
		}
		colorIndicator = new Texture2D(1, 1, TextureFormat.RGBA32, false);
		colorIndicator.filterMode = FilterMode.Point;
		Color[] palCols = new Color[colorIndicator.width*colorIndicator.height];
		for(int i = 0; i < palCols.Length; i++) {
			palCols[i] = palette[paletteIndex];
		}
		colorIndicator.SetPixels(palCols);
		colorIndicator.Apply();
		New (w, h);
		sliderThumb = new Texture2D(16, 16);
		//preview.transform.parent.position = cam.ViewportToWorldPoint(new Vector3(0,1,-cam.transform.position.z));
		//preview.transform.parent.position = cam.ViewportToWorldPoint(new Vector3(0.5f,1,-cam.transform.position.z));
		//preview.transform.parent.localScale = Vector3.one * w / ((cam.pixelHeight * cam.aspect) / (cam.orthographicSize * cam.aspect * 2)) * 4;
		if(overlay) {
			overlay.transform.parent.localScale = new Vector3(h * cam.aspect * 2, h * 2, 1);
			overlay.transform.parent.position = cam.ViewportToWorldPoint(new Vector3(0.5f,0.5f,-cam.transform.position.z-1));
			overlay.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
		if(overlay && overlay.enabled) {
			return;
		}

		targetZoom -= Input.GetAxis("Mouse ScrollWheel") * 10;
		cam.orthographicSize += (targetZoom - cam.orthographicSize) * Time.deltaTime * 10;

		if(Input.GetMouseButtonDown(2)) {
			dragOrigin = cam.ScreenToWorldPoint(Input.mousePosition);
		}
		if(Input.GetMouseButton(2)) {
			Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
			cam.transform.position += dragOrigin - p;
		}

		if(paintMode == paintModes.Brush) {
			if(Input.GetMouseButton(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				//canvas.SetPixel(x, y, Color.black);
				//canvas.Apply();
				if(!(lastX == x && lastY == y) && (x >= 0 && x < canvas.width && y >= 0 && y < canvas.height)) {
					lastX = x;
					lastY = y;
					Draw(x, y, brushRadius, palette[paletteIndex]);
				}
			}
		}
		if(paintMode == paintModes.Flood) {
			if(Input.GetMouseButtonDown(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				if(x < canvas.width && y >= 0 && y < canvas.height) {
					Flood(x, y);
				}
			}
		}

		if(Input.GetMouseButtonUp(0)) {
			lastX = -1;
			lastY = -1;
		}
		//TODO: pinch zoom (smooth), and two finger pan

		if(Input.GetKeyDown(KeyCode.Space)) {
			Test();
		}
		if(Input.GetKeyDown(KeyCode.LeftBracket)) {
			brushRadius--;
			brushRadius = Mathf.Max(brushRadius, 1);
		}
		if(Input.GetKeyDown(KeyCode.RightBracket)) {
			brushRadius++;
			brushRadius = Mathf.Min(brushRadius, 9);
		}
		if(Input.GetKeyDown(KeyCode.Comma)) {
			paletteIndex--;
			while (paletteIndex < 0) {
				paletteIndex += 16;
			}
			Color[] palCols = new Color[colorIndicator.width*colorIndicator.height];
			for(int i = 0; i < palCols.Length; i++) {
				palCols[i] = palette[paletteIndex];
			}
			colorIndicator.SetPixels(palCols);
			colorIndicator.Apply();
		}
		if(Input.GetKeyDown(KeyCode.Period)) {
			paletteIndex++;
			while(paletteIndex > 15) {
				paletteIndex -= 16;
			}
			Color[] palCols = new Color[colorIndicator.width*colorIndicator.height];
			for(int i = 0; i < palCols.Length; i++) {
				palCols[i] = palette[paletteIndex];
			}
			colorIndicator.SetPixels(palCols);
			colorIndicator.Apply();
		}
	}

	private void UpdatePalette(int idx) {
		Color[] cols = canvas.GetPixels();
		for(int i = 0; i < paletteIndices.Length; i++) {
			if(paletteIndices[i] == idx) {
				cols[i] = palette[idx];
			}
		}
		canvas.SetPixels(cols);
		canvas.Apply();
	}

	private void UpdateColorIndicator() {
		Color[] palCols = new Color[colorIndicator.width*colorIndicator.height];
		for(int i = 0; i < palCols.Length; i++) {
			palCols[i] = palette[paletteIndex];
		}
		colorIndicator.SetPixels(palCols);
		colorIndicator.Apply();
	}
	
	private void Draw(int xCoord, int yCoord, int radius, Color col) {
		Color[] cols = canvas.GetPixels();
		for(int y = 0; y < canvas.height; y++) {
			for(int x = 0; x < canvas.width; x++) {
				int dy = y - yCoord;
				int dx = x - xCoord;
				if(dy*dy+dx*dx < radius*radius) {
					//get pixel's existing palette index vs painted palette index, anti-alias via ratio of difference between
					//ie. existing index: 5, new index: 10, anti-alias: 50%, final palette index: 7 or 8
					int i = y * canvas.height + x;
					int p0 = paletteIndices[i];
					int p1 = paletteIndex;
					int p = Mathf.RoundToInt(Mathf.Lerp(p0, p1, 1-Mathf.Clamp01((float)(dy*dy+dx*dx)/(radius*radius))));
					//int p = Mathf.RoundToInt(Mathf.Lerp(p0, p1, 1-(Mathf.Sqrt(dy*dy+dx*dx)/radius)));
					//cols[i] = Color.Lerp(cols[i], col, 1-(float)(dy*dy+dx*dx)/(radius*radius));
					cols[i] = palette[p];
					paletteIndices[i] = p;
				}
			}
		}
		canvas.SetPixels(cols);
		canvas.Apply();
	}
	
	private void Flood(int xCoord, int yCoord) {
		Color[] cols = canvas.GetPixels();
		//Color col = cols[yCoord * canvas.height + xCoord];
		int colIndex = paletteIndices[yCoord * canvas.height + xCoord];
		List<int> processed = new List<int>();
		FloodStep(xCoord, yCoord, colIndex, ref cols, ref processed);
		canvas.SetPixels(cols);
		canvas.Apply();
	}
	
	private void FloodStep(int xCoord, int yCoord, int colIndex, ref Color[] cols, ref List<int> processed) {
		if(xCoord < 0 || xCoord > canvas.width-1 || yCoord < 0 || yCoord > canvas.height-1) {
			return;
		}
		int idx = yCoord * canvas.height + xCoord;
		if(processed.Contains(idx)) {
			return;
		}
		//if(cols[idx] != col) {
		if(Mathf.Abs(paletteIndices[idx] - colIndex) > (int)(floodTolerance * palette.Length)) {
		//if(paletteIndices[idx] != colIndex) {
			return;
		}
		cols[idx] = palette[paletteIndex];
		paletteIndices[idx] = paletteIndex;
		processed.Add(idx);
		FloodStep(xCoord+1, yCoord, colIndex, ref cols, ref processed);
		FloodStep(xCoord-1, yCoord, colIndex, ref cols, ref processed);
		FloodStep(xCoord, yCoord+1, colIndex, ref cols, ref processed);
		FloodStep(xCoord, yCoord-1, colIndex, ref cols, ref processed);
	}

	private void Test() {
		for(int x = 0; x < canvas.width; x++) {
			int y = Mathf.FloorToInt((Mathf.Sin(((float)x / canvas.width) * Mathf.PI * 2) *0.5f+0.5f) * canvas.height);
			canvas.SetPixel(x, y, Color.black);
			canvas.SetPixel(x, y-1, Color.black);
			canvas.SetPixel(x, y+1, Color.black);
		}
		canvas.Apply();
	}

	public void New(int w, int h) {
		canvas = new Texture2D(w, h, TextureFormat.ARGB32, false);
		canvas.filterMode = FilterMode.Point;
		canvas.wrapMode = TextureWrapMode.Clamp;
		paletteIndices = new int[canvas.width*canvas.height];
		Color[] cols = new Color[canvas.width*canvas.height];
		for(int i = 0; i < paletteIndices.Length; i++) {
			paletteIndices[i] = palette.Length-1;
			cols[i] = palette[paletteIndices[i]];
		}
		canvas.SetPixels(cols);
		canvas.Apply();
		quad.material.mainTexture = canvas;
		preview.material.mainTexture = canvas;
		quad.transform.parent.localScale = Vector3.one * Mathf.Min(w, h);
		//cam.orthographicSize = (Mathf.Min(w, h) * 0.5f) / cam.aspect;
		targetZoom = cam.orthographicSize = Mathf.Min(w, h);
		cam.transform.position = new Vector3(w * 0.5f, h * 0.25f, -10);
		//cam.orthographicSize = cam.pixelHeight;
	}

	void OnGUI() {
		float dim = cam.pixelWidth / 4;
		float mod = 1;
		skin.horizontalSliderThumb.normal.background = sliderThumb;
		skin.horizontalSliderThumb.active.background = sliderThumb;
		skin.horizontalSliderThumb.hover.background = sliderThumb;
		skin.verticalSliderThumb.normal.background = sliderThumb;
		skin.verticalSliderThumb.active.background = sliderThumb;
		skin.verticalSliderThumb.hover.background = sliderThumb;
		skin.button.fontSize = (int)(dim/2);
		skin.label.fontSize = (int)(dim/4);
		skin.button.margin = new RectOffset(0,0,0,0);
		skin.label.margin = new RectOffset(0,0,0,0);

		skin2.button.normal.background = colorIndicator;
		skin2.button.active.background = colorIndicator;
		skin2.button.hover.background = colorIndicator;
		//skin.horizontalSlider.margin = new RectOffset(0,0,0,0);
		GUI.skin = skin;

		if(GUI.Button(new Rect(0,0,dim/2,dim/2), "*")) {
			currentUI++;
			if(currentUI > uis.Count) {
				currentUI = 0;
			}
			if(currentUI == uis.Count) {
				overlay.enabled = false;
			}
			else {
				overlay.enabled = true;
				overlay.material.mainTexture = uis[currentUI];
			}
		}
		if(overlay && overlay.enabled) {
			return;
		}

		GUILayout.BeginArea(new Rect(0, cam.pixelHeight * 0.666f, cam.pixelWidth, cam.pixelHeight*0.333f));
		GUILayout.BeginHorizontal();
		if(GUILayout.Button("-", GUILayout.Width(dim), GUILayout.Height(dim))) {
			brushRadius--;
			brushRadius = Mathf.Max(brushRadius, 1);
		}
		GUILayout.Label(brushRadius + "px", GUILayout.Width(dim), GUILayout.Height(dim));
		if(GUILayout.Button("+", GUILayout.Width(dim), GUILayout.Height(dim))) {
			brushRadius++;
			brushRadius = Mathf.Max(brushRadius, 1);
		}
		GUILayout.BeginVertical();
		if(GUILayout.Button(paintMode.ToString()[0].ToString(), GUILayout.Width(dim), GUILayout.Height(paintMode == paintModes.Flood ? dim*2/3 : dim))) {
			paintMode++;
			if(paintMode == paintModes.COUNT) {
				paintMode = 0;
			}
		}
		if(paintMode == paintModes.Flood) {
			GUILayout.BeginHorizontal();
			GUILayout.Label(((int)(floodTolerance * palette.Length)).ToString(), GUILayout.Width(dim/3));
			floodTolerance = GUILayout.HorizontalSlider(floodTolerance, 0, 1, GUILayout.Width(dim*2/3), GUILayout.Height(dim/3));
			GUILayout.EndHorizontal();
		}
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.Space(10*mod);
		GUILayout.BeginHorizontal();
		if(GUILayout.Button("<", GUILayout.Width(dim), GUILayout.Height(dim))) {
			paletteIndex--;
			while (paletteIndex < 0) {
				paletteIndex += palette.Length;
			}
			UpdateColorIndicator();
		}
		/*
		if(!editingPalette) {
			if(GUILayout.Button(colorIndicator, GUILayout.Width(Screen.width - dim*2), GUILayout.Height(dim))) {
				editingPalette = !editingPalette;
			}
		}
		*/
		Color c = palette[paletteIndex];
		GUILayout.BeginVertical();
		GUI.changed = false;
		c.r = GUILayout.HorizontalSlider(c.r, 0, 1, GUILayout.Height(dim/3));
		c.g = GUILayout.HorizontalSlider(c.g, 0, 1, GUILayout.Height(dim/3));
		c.b = GUILayout.HorizontalSlider(c.b, 0, 1, GUILayout.Height(dim/3));
		if(GUI.changed) {
			palette[paletteIndex] = c;
			UpdatePalette(paletteIndex);
			UpdateColorIndicator();
		}
		GUILayout.EndVertical();
		GUILayout.Button("", skin2.button, GUILayout.Width(dim*2/3), GUILayout.Height(dim));

		if(GUILayout.Button(">", GUILayout.Width(dim), GUILayout.Height(dim))) {
			paletteIndex++;
			while(paletteIndex > palette.Length-1) {
				paletteIndex -= palette.Length;
			}
			UpdateColorIndicator();
		}
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}
}
