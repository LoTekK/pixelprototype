﻿using UnityEngine;
using System.Collections;

public static class MathHelper {

	public static float Remap(float val, float oldMin, float oldMax, float newMin, float newMax) {
		return (val - oldMin) / (oldMax - oldMin) * (newMax - newMin) + newMin;
	}

	public static Color Remap(Color col, float oldMin, float oldMax, float newMin, float newMax) {
		for(int i = 0; i < 3; i++) {
			col[i] = Remap(col[i], oldMin, oldMax, newMin, newMax);
		}
		return col;
	}

	public static Color Desaturate(Color col) {
		col.r = col.g = col.b = ColorToLuminance(col);
		return col;
	}

	public static float ColorToLuminance(Color col) {
		return col.r*0.299f + col.g*0.587f + col.b*0.114f;
	}
}
