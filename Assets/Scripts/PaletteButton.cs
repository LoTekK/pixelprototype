﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class PaletteButton : Button {

	public int paletteIndex;

	public override void OnPointerClick (UnityEngine.EventSystems.PointerEventData eventData)
	{
		base.OnPointerClick (eventData);
		PixelEdit.Instance.SetPalette(paletteIndex);
	}
}
