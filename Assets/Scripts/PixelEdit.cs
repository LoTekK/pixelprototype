﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Gif.Components;

//TODO: rework actual canvas stuff
//instead of constantly doing GetPixels, just store the canvas as a flat array, then we only need to SetPixels after modifying
//additionally, move to Get/SetPixels32
//frames should be an array of canvas arrays (canvas class?)

//TODO: canvas class/container, also get rid of tex2d ops in the draw/flood ops, just modify array, set dirty
//TODO: modify above to use the buffer array instead of modifying canvas directly. Then apply buffer to canvas after setting dirty
//TODO: add user-modifiable palette limitations (eg. C64 double-wide pixels, and only 3 colors per 2x2)
//	good ref for above: http://wayofthepixel.net/index.php?topic=10784.0

//TODO: make Frame class? save all the fucking index dancing...
//and layer class

[System.Serializable]
public class Col32 {
	public byte r, g, b, a;
	public Color32 ToColor32 {
		get {
			return new Color32(r, g, b, a);
		}
	}

	public static implicit operator Color32(Col32 c) {
		return new Color32(c.r, c.g, c.b, c.a);
	}

	public Col32(Color32 col) {
		r = col.r;
		g = col.g;
		b = col.b;
		a = col.a;
	}
}

/*
[System.Serializable]
public class PxCanvas {
	public Col32[] paletteColors { get; private set; }
	public int[] paletteIndices { get; private set; }
	public Color32[] paletteColors32 {
		get {
			Color32[] cols = new Color32[paletteColors.Length];
			for(int i = 0; i < cols.Length; i++) {
				cols[i] = paletteColors[i].ToColor32;
			}
			return cols;
		}
	}
	public Stack<Action> UndoStack { get; private set; }
	public Stack<Action> RedoStack { get; private set; }

	public PxCanvas(Color32[] cols, int[] indices, Stack<Action> undoStack, Stack<Action> redoStack) {
		paletteColors = new Col32[cols.Length];
		for(int i = 0; i < paletteColors.Length; i++) {
			paletteColors[i] = new Col32(cols[i]);
		}
		paletteIndices = indices;
		UndoStack = undoStack;
		RedoStack = redoStack;
	}
}
*/
[System.Serializable]
public class PxCanvas {
	public int[] canvasSize { get; private set; }
	public Col32[] paletteColors { get; private set; }
	//public int[] paletteIndices { get; private set; }
	public List<int[]> frameIndices = new List<int[]>();
	public Color32[] paletteColors32 {
		get {
			Color32[] cols = new Color32[paletteColors.Length];
			for(int i = 0; i < cols.Length; i++) {
				cols[i] = paletteColors[i];//.ToColor32;
			}
			return cols;
		}
	}
	public int fgColor;
	public int bgColor;
	public int currentFrame;
	public Stack<UndoStep> UndoStack { get; private set; }
	public Stack<UndoStep> RedoStack { get; private set; }

	public PxCanvas(Color32[] cols, List<int[]> indices, Stack<UndoStep> undoStack, Stack<UndoStep> redoStack, int canvasWidth, int canvasHeight, int fg, int bg, int frame) {
		paletteColors = new Col32[cols.Length];
		for(int i = 0; i < paletteColors.Length; i++) {
			paletteColors[i] = new Col32(cols[i]);
		}
		//paletteIndices = indices;
		frameIndices = indices;
		UndoStack = undoStack;
		RedoStack = redoStack;
		canvasSize = new int[] { canvasWidth, canvasHeight };
		fgColor = fg;
		bgColor = bg;
		currentFrame = frame;
	}
}

public class Buffer {
	//use direct palette index here, instead of delta
	//-1 indicates empty pixel in buffer (don't do anything)
	//when storing undo, calculate and store delta instead of direct
	public int[] indices { get; private set; }
	public int width, height;

	public Buffer(int w, int h) {
		width = w;
		height = h;
		indices = new int[width * height];
	}

	public void Clear() {
		for(int i = 0; i < indices.Length; i++) {
			indices[i] = -1;
		}
	}

	public void Flush(ref int[] paletteIndices) {
		for(int i = 0; i < indices.Length; i++) {
			paletteIndices[i] = indices[i];
		}
		Clear();
	}

	public int GetX(int i) {
		return indices[i % width];
	}
	public int GetY(int i) {
		return indices[i / width];
	}
}

/*
 * 	private void ClearBuffer() {
		for(int i = 0; i < buffer.Length; i++) {
			buffer[i] = 0;
		}
		SetDirty(true);
	}

	private void FlushBuffer() {
		for(int i = 0; i < buffer.Length; i++) {
			paletteIndices[i] += buffer[i];	//buffer will store the delta palette index
		}
		if(UndoStack.Count > 0) {
			bool undo = UndoStack.Peek().FromBuffer(buffer);
			if(!undo) {
				UndoStack.Pop();
			}
		}
		ClearBuffer();
	}

*/

public class Frame {
	public Texture2D tex;
	public int[] pixels;
	//public 
}

public class PixelEdit : Singleton<PixelEdit> {

	private Camera cam;
	public int exportScale = 4;
	//private Texture2D canvas;
	private bool _showOnionSkin = true;
	public bool showOnionSkin {
		get {
			return _showOnionSkin;
		}
		set {
			_showOnionSkin = value;
			SetDirty(true);
		}
	}
	private bool onionWrap = true;
	private int _currentFrame;
	public int currentFrame {
		get {
			_currentFrame = Mathf.Clamp(_currentFrame, 0, frames.Count-1);
			return _currentFrame;
		}
		set {
			_currentFrame = (int)Mathf.Repeat(value, frames.Count);
			quad.material.mainTexture = frames[_currentFrame];
			SetDirty(true);
		}
	}
	private int prevFrame {
		get {
			return onionWrap ? (int)Mathf.Repeat(currentFrame-1, frames.Count) : Mathf.Clamp(currentFrame-1, 0, frames.Count);
		}
	}
	private int nextFrame {
		get {
			return onionWrap ? (int)Mathf.Repeat(currentFrame+1, frames.Count) : Mathf.Clamp(currentFrame+1, 0, frames.Count);
		}
	}

	private int FramePlus(int delta) {
		return onionWrap ? (int)Mathf.Repeat(currentFrame + delta, frames.Count) : Mathf.Clamp(currentFrame+delta, 0, frames.Count-1);
	}

	private Texture2D canvas {
		get {
			if(frames.Count == 0) {
				frames.Add(new Texture2D(w,h));
			}
			return frames[currentFrame];
		}
		set {
			if(frames.Count == 0) {
				frames.Add(value);
			}
			else {
				frames[currentFrame] = value;
			}
		}
	}
	public List<Texture2D> frames = new List<Texture2D>();
	public Renderer grid;
	public Renderer quad;
	public Renderer onionSkin;
	private Texture2D onionSkinTex;
	public int onionSteps = 2;
	public RawImage preview;
	public Button paletteEntryPrefab;
	public GridLayoutGroup paletteShelf;
	public List<PaletteButton> paletteEntries = new List<PaletteButton>();
	public Transform palettePointer;
	public Transform palettePointerBg;
	public GridLayoutGroup toolShelf;
	public Button[] toolButtons;
	private float _brushRadius = 1;
	public float brushRadius {
		get {
			return _brushRadius;
		}
		set {
			_brushRadius = value;
		}
	}
	private int lastX;
	private int lastY;
	private int selX;
	private int selY;
	public int w = 32;
	public int h = 32;
	public int paletteCount;
	public Texture2D paletteTex;
	public Color32[] palette;
	public int paletteIndex = 0;
	private Texture2D colorIndicator;
	public Image colorButton;
	public ColorGui colorGui;
	private float previewFramerate = 10;

	//track this instead of using canvas.GetPixels
	//potentially keep an array per palette entry? with pixel index or coord?
	//public int[] paletteIndices;
	public int[] paletteIndices {
		get {
			if(frameIndices.Count == 0) {
				frameIndices.Add(new int[canvas.width * canvas.height]);
			}
			return frameIndices[currentFrame];
		}
		set {
			if(frameIndices.Count == 0) {
				frameIndices.Add(value);
			}
			else {
				frameIndices[currentFrame] = value;
			}
		}
	}
	public List<int[]> frameIndices = new List<int[]>();
	//public int[] selection;
	public List<int> selection = new List<int>();
	public List<int> tempSelection = new List<int>();
	public int[] buffer;
	public int[] selectionBuffer;
	public int[] sortedPalette;
	public bool isDirty = true;
	public GUISkin skin;
	public GUISkin skin2;
	private Texture2D sliderThumb;
	public enum paintModes {
		Brush,
		Flood,
		Move,
		Clone,
		Eyedropper,
		Select,
		Line,
		MirrorH,
		Ellipse,
		COUNT
	}
	private paintModes paintMode;
	private float _floodTolerance = 0;
	public float floodTolerance {
		get {
			return _floodTolerance;
		}
		set {
			_floodTolerance = value;
		}
	}
	public Renderer overlay;
	public List<Texture2D> uis = new List<Texture2D>();
	private int currentUI = -1;
	private float targetZoom;
	private Vector3 dragOrigin;
	private bool _antiAlias;
	public bool antiAlias {
		get {
			return _antiAlias;
		}
		set {
			_antiAlias = value;
		}
	}
	public Slider brushRadiusSlider;
	public Slider floodToleranceSlider;
	private bool firstTouch;

	public UnityEngine.UI.InputField filenameInput;

	//public Stack<Action> UndoStack = new Stack<Action>();
	//public Stack<Action> RedoStack = new Stack<Action>();

	public Stack<UndoStep> UndoStack = new Stack<UndoStep>();
	public Stack<UndoStep> RedoStack = new Stack<UndoStep>();

	//if we pass in an index, also pass out what its new [sorted] index is
	//alternately, we return int[,] (or int[][]), with original and sorted indices
	//better yet, simple int array, order remains same, but indices point to sorted
	public int[] SortedPalette() {
		int[] sortedPalette = new int[palette.Length];
		for(int i = 0; i < sortedPalette.Length; i++) {
			sortedPalette[i] = i;
		}
		System.Array.Sort(sortedPalette, delegate(int i0, int i1) {
			//return user1.Age.CompareTo(user2.Age); // (user1.Age - user2.Age)
			float v0 = Utils.GetValue(palette[i0]);
			float v1 = Utils.GetValue(palette[i1]);
			float s0 = Utils.GetSaturation(palette[i0]);
			float s1 = Utils.GetSaturation(palette[i1]);
			float h0 = Utils.GetHue(palette[i0]);
			float h1 = Utils.GetHue(palette[i1]);
			int v = v0.CompareTo(v1);
			int s = s0.CompareTo(s1);
			int h = h0.CompareTo(h1);
			int i = i0.CompareTo(i1);
			//return (Utils.GetValue(palette[i0]).CompareTo(Utils.GetValue(palette[i1])).CompareTo(Utils.GetSaturation(palette[i0]).CompareTo(Utils.GetSaturation(palette[i1]))));
			//Debug.Log(i0 + " : " + i1 + " : " + v0.CompareTo(v1) + " : " + s0.CompareTo(s1) + " : " + h0.CompareTo(h1) + " : " + i0.CompareTo(i1));
			//return (((v0.CompareTo(v1)).CompareTo(s0.CompareTo(s1))).CompareTo(h0.CompareTo(h1)));
			return v != 0 ? v : s != 0 ? s : h != 0 ? h : i;
		});
		return sortedPalette;
	}

	public void SetCurrentAsBgColor() {
		paletteShelf.transform.parent.gameObject.SetActive(true);
		bgColor = paletteIndex;
	}

	private int _bgColor = -1;
	public int bgColor {	//TODO: hardcoding this to last palette entry. Should allow user-defined
		get {
			if(_bgColor < 0) {
				return palette.Length-1;
			}
			else {
				return _bgColor;
			}
		}
		set {
			_bgColor = value;
			palettePointerBg.SetParent(paletteEntries[value].transform);
			palettePointerBg.localPosition = Vector3.zero;
		}
	}

	public void HideColorWheel() {
		paletteShelf.transform.parent.gameObject.SetActive(true);
	}
	
	public void SetPalette(int n) {
		if(n == paletteIndex) {
			paletteShelf.transform.parent.gameObject.SetActive(false);
			return;
		}
		paletteIndex = n;
		palettePointer.position = paletteEntries[paletteIndex].transform.position - Camera.main.transform.forward;
		palettePointer.SetParent(paletteEntries[paletteIndex].transform);
		//colorButton.color = palette[n];
		colorGui.col = palette[n];
		colorGui.UpdateHSV();
	}

	public void FrameNext() {
		currentFrame++;
	}

	public void FramePrev() {
		currentFrame--;
	}

	public void Save() {
		string filename = filenameInput.value;
		if(string.IsNullOrEmpty(filename)) {
			return;
		}
		PxCanvas p = new PxCanvas(palette, frameIndices, UndoStack, RedoStack, w, h, paletteIndex, bgColor, currentFrame);
		BinaryFormatter b = new BinaryFormatter();
		FileStream f = File.Create(Application.persistentDataPath + "/" + filename + ".pxc");
		//b.Serialize(f, p.frameIndices);
		b.Serialize(f, p);
		f.Close();
		Color32[] cols = new Color32[w*exportScale * h*exportScale];
		for(int y = 0; y < h*exportScale; y++) {
			for(int x = 0; x < w*exportScale; x++) {
				int newi = y*w*exportScale + x;
				int i = (int)(y/exportScale)*w + (int)(x/exportScale);
				cols[newi] = palette[paletteIndices[i]]; 
			}
		}
		Texture2D forExport = new Texture2D(w*exportScale, h*exportScale, TextureFormat.ARGB32, false);
		forExport.filterMode = FilterMode.Point;
		forExport.wrapMode = TextureWrapMode.Clamp;
		forExport.SetPixels32(cols);
		forExport.Apply();
		byte[] export = forExport.EncodeToPNG();//canvas.EncodeToPNG();
		File.WriteAllBytes(Application.persistentDataPath + "/" + filename + ".png", export);
		if(frames.Count > 1) {
			ExportGIF();
		}
		//ExportReplayGIF();
	}

	public void Load() {
		string filename = filenameInput.value;
		if(string.IsNullOrEmpty(filename)) {
			return;
		}
		BinaryFormatter b = new BinaryFormatter();
		FileStream f = File.Open(Application.persistentDataPath + "/" + filename + ".pxc", FileMode.Open);
		//int[] p = (int[])b.Deserialize(f);
		//p.CopyTo(paletteIndices, 0);
		PxCanvas p = (PxCanvas)b.Deserialize(f);
		int[] canvasSize = p.canvasSize;
		New(canvasSize[0], canvasSize[1]);
		frames.Clear();
		frameIndices = p.frameIndices;
		palette = p.paletteColors32;
		UpdatePalette();
		paletteIndex = -1;
		SetPalette(p.fgColor);
		bgColor = p.bgColor;
		while(frames.Count < frameIndices.Count) {
			frames.Add(NewTex());
		}
		currentFrame = 0;
		ClearBuffer();
		for(int i = 0; i < frames.Count; i++) {
			WriteFrame(i);
		}
		UpdatePalette();
		currentFrame = p.currentFrame;
		f.Close();
		Stack<UndoStep> u = p.UndoStack;
		UndoStack = u == null ? new Stack<UndoStep>() : u;
		Stack<UndoStep> r = p.RedoStack;
		RedoStack = r == null ? new Stack<UndoStep>() : r;
		SetDirty(true);
	}

	public void WriteFrame(int frame) {
		Color32[] cols = new Color32[frameIndices[frame].Length];
		for(int i = 0; i < cols.Length; i++) {
			//cols[i] = palette[paletteIndices[i] + buffer[i]];
			try {
				cols[i] = buffer[i] < 0 ? palette[frameIndices[frame][i]] : palette[buffer[i]];
			}
			catch {
				Debug.Log("Index: " + i + " - Palette: " + frameIndices[frame][i]);
			}
		}
		/*
		foreach(int i in selection) {
			cols[i] = Color32.Lerp(cols[i], cols[i].Luminance() > 127 ? Color.black : Color.white, 0.333f);
		}
		*/
		frames[frame].SetPixels32(cols);
		frames[frame].Apply();
	}

	public Texture2D NewTex() {
		Texture2D t = new Texture2D(w, h, TextureFormat.ARGB32, false);
		t.wrapMode = TextureWrapMode.Clamp;
		t.filterMode = FilterMode.Point;
		Color32[] cols = new Color32[w*h];
		for(int i = 0; i < w*h; i++) {
			cols[i] = palette[bgColor];
		}
		t.SetPixels32(cols);
		t.Apply();
		return t;
	}

	public void Replay(bool exportGif = true) {	//TODO: need to make sure this takes into account canvas size changes
		int undoCount = UndoStack.Count;
		while(UndoStack.Count > 0) {
			UndoStep u = UndoStack.Pop();
			u.Undo(this);
			RedoStack.Push(u);
		}
		//New(w, h);
		if(exportGif) {
			StartCoroutine(ReplayGIFCR(undoCount));
		}
		else {
			StartCoroutine(ReplayCR(undoCount));
		}
	}

	IEnumerator ReplayCR(int undoCount) {
		yield return null;
		//while(RedoStack.Count >= undoCount) {
		while(UndoStack.Count < undoCount) {
			Redo();
			yield return null;
		}
	}
	
	IEnumerator ReplayGIFCR(int undoCount) {
		AnimatedGifEncoder e = new AnimatedGifEncoder();
		e.Start( Application.persistentDataPath + "/" + filenameInput.value + "_REPLAY.gif" );
		e.SetDelay(33);
		//-1:no repeat,0:always repeat
		e.SetRepeat(0);

		yield return null;
		int[] forExport = new int[w*exportScale * h*exportScale];
		//while(RedoStack.Count >= undoCount) {
		while(UndoStack.Count < undoCount) {
			Redo();
			//e.AddFrame(canvas);
			for(int y = 0; y < h*exportScale; y++) {
				for(int x = 0; x < w*exportScale; x++) {
					int newi = y*w*exportScale + x;
					int i = (int)(y/exportScale)*w + (int)(x/exportScale);
					forExport[newi] = paletteIndices[i];
				}
			}
			
			e.AddFrame(palette, forExport, w * exportScale, h * exportScale);
			yield return null;
		}
		e.SetDelay(1000);
		//e.AddFrame(canvas);	//add a hold on the final frame
		for(int y = 0; y < h*exportScale; y++) {
			for(int x = 0; x < w*exportScale; x++) {
				int newi = y*w*exportScale + x;
				int i = (int)(y/exportScale)*w + (int)(x/exportScale);
				forExport[newi] = paletteIndices[i];
			}
		}
		
		e.AddFrame(palette, forExport, w * exportScale, h * exportScale);
		e.Finish();
	}
	
	public void Undo() {
		if(UndoStack.Count == 0) {
			return;
		}
		//Action a = UndoStack.Pop();
		UndoStep a = UndoStack.Pop();
		a.Undo(this);
		RedoStack.Push(a);
		//Debug.Log(UndoStack.Count + " : " + RedoStack.Count);
	}

	public void Redo() {
		if(RedoStack.Count == 0) {
			return;
		}
		//Action a = RedoStack.Pop();
		UndoStep a = RedoStack.Pop();
		a.Redo(this);
		UndoStack.Push(a);
	}

	public void ExportGIF() {
		AnimatedGifEncoder e = new AnimatedGifEncoder();
		e.Start( Application.persistentDataPath + "/" + filenameInput.value + ".gif" );
		e.SetDelay(100);
		//-1:no repeat,0:always repeat
		e.SetRepeat(0);
		/*
		foreach(Texture2D f in frames) {
			e.AddFrame( f );
		}
		*/
		foreach(int[] indices in frameIndices) {
			int[] forExport = new int[w*exportScale * h*exportScale];
			for(int y = 0; y < h*exportScale; y++) {
				for(int x = 0; x < w*exportScale; x++) {
					int newi = y*w*exportScale + x;
					int i = (int)(y/exportScale)*w + (int)(x/exportScale);
					forExport[newi] = indices[i];
				}
			}
			
			e.AddFrame(palette, forExport, w * exportScale, h * exportScale);
		}
		e.Finish();
	}

	// Use this for initialization
	IEnumerator Start () {
		cam = Camera.main;
		onionSkinTex = new Texture2D(w, h, TextureFormat.ARGB32, false);
		onionSkinTex.filterMode = FilterMode.Point;
		onionSkinTex.wrapMode = TextureWrapMode.Clamp;
		onionSkin.material.mainTexture = onionSkinTex;
		palette = new Color32[paletteCount];
		floodToleranceSlider.maxValue = paletteCount-1;
		for(int i = 0; i < paletteCount; i++) {
			palette[i] = paletteTex.GetPixel(Mathf.FloorToInt((i+0.5f) * paletteTex.width / paletteCount), 0);
			//byte c = (byte)(Mathf.Clamp01((float)i/(paletteCount-1)) * 255);
			//palette[i] = new Color32(c,c,c,255);
		}
		sortedPalette = SortedPalette();
		Color32[] tempPal = new Color32[paletteCount];
		palette.CopyTo(tempPal, 0);
		for(int i = 0; i < paletteCount; i++) {
			palette[i] = tempPal[sortedPalette[i]];
		}
		colorIndicator = new Texture2D(1, 1, TextureFormat.RGBA32, false);
		colorIndicator.filterMode = FilterMode.Point;
		Color32[] palCols = new Color32[colorIndicator.width*colorIndicator.height];
		for(int i = 0; i < palCols.Length; i++) {
			palCols[i] = palette[paletteIndex];
		}
		colorIndicator.SetPixels32(palCols);
		colorIndicator.Apply();
		New (w, h);
		sliderThumb = new Texture2D(16, 16);
		//preview.transform.parent.position = cam.ViewportToWorldPoint(new Vector3(0,1,-cam.transform.position.z));
		//preview.transform.parent.position = cam.ViewportToWorldPoint(new Vector3(0.5f,1,-cam.transform.position.z));
		//preview.transform.parent.localScale = Vector3.one * w / ((cam.pixelHeight * cam.aspect) / (cam.orthographicSize * cam.aspect * 2)) * 4;
		if(overlay) {
			overlay.transform.parent.localScale = new Vector3(h * cam.aspect * 2, h * 2, 1);
			overlay.transform.parent.position = cam.ViewportToWorldPoint(new Vector3(0.5f,0.5f,-cam.transform.position.z-1));
			overlay.enabled = false;
		}
		/*
		for(int i = 0; i < palette.Length; i++) {
			Button b = (Button)Instantiate(paletteEntryPrefab);
			b.transform.parent = paletteShelf.transform;
			b.transform.localScale = Vector3.one;
			b.onClick.AddListener( SetPalette(i) );
			b.image.color = palette[i];
		}
		*/
		//paletteShelf.GetComponent<RectTransform>().offsetMax = Vector2.right * palette.Length * (paletteEntryPrefab.GetComponent<RectTransform>().offsetMax.x - paletteEntryPrefab.GetComponent<RectTransform>().offsetMin.x);
		for(int i = 0; i < palette.Length; i++) {
			PaletteButton b = Instantiate(paletteEntryPrefab) as PaletteButton;
			//b.onClick.AddListener( delegate { SetPalette(i); });	//passing in a direct value works, but not a local var?
			b.transform.parent = paletteShelf.transform;
			b.transform.localScale = Vector3.one;
			b.paletteIndex = i;
			b.image.color = palette[i];
			paletteEntries.Add(b);
		}
		toolButtons = toolShelf.GetComponentsInChildren<Button>();
		yield return null;	//bit dirty, but the gridlayout appears to wait for end of frame
		SetMode((int)paintModes.Brush);
		SetPalette(0);
		paletteShelf.transform.parent.gameObject.SetActive(true);
	}

	public void SetDirty(bool dirty) {
		isDirty = dirty;
	}

	public void ResizeCanvas(RectOffset offset, bool undoAction = false) {	//+ve expand, -ve contract
		if(!undoAction) {
			UndoStep u = new UndoStep();
			u.ResizeCanvas(offset, frameIndices);
			UndoStack.Push(u);
			RedoStack.Clear();
		}
		int newW = w + offset.horizontal;
		int newH = h + offset.vertical;
		//Debug.Log("w: " + w + "," + newW + " h: " + h + "," + newH);
		List<int[]> tempFrames = new List<int[]>();
		for(int i = 0; i < frameIndices.Count; i++) {
			int[] tempFrame = new int[newW*newH];
			for(int y = 0; y < newH; y++) {
				for(int x = 0; x < newW; x++) {
					int oldY = y-offset.bottom;
					int oldX = x-offset.left;
					int idx = oldY*w + oldX;
					tempFrame[y*newW + x] = oldY < 0 || oldY >= h || oldX < 0 || oldX >= w ? bgColor : frameIndices[i][oldY*w + oldX];
				}
			}
			tempFrames.Add(tempFrame);
		}
		w = newW;
		h = newH;
		New (w, h);
		frames.Clear();
		frameIndices.Clear();
		//frameIndices = (List<int[]>)b.Deserialize(f);
		foreach(int[] i in tempFrames) {
			frames.Add(NewTex());
			frameIndices.Add(i);
		}
		int originalFrame = currentFrame;
		currentFrame = 0;
		ClearBuffer();
		for(int i = 0; i < frames.Count; i++) {
			WriteFrame(i);
		}
		currentFrame = originalFrame;
	}

	public void CropToSelection() {
		if(selection.Count == 0) {
			return;
		}
		int xMin = w;
		int yMin = h;
		int xMax = 0;
		int yMax = 0;
		foreach(int i in selection) {
			int x = i % w;
			int y = i / w;
			if(x < xMin) xMin = x;
			if(x > xMax) xMax = x;
			if(y < yMin) yMin = y;
			if(y > yMax) yMax = y;
		}
		//int newW = xMax - xMin;
		//int newH = yMax - yMin;
		selection.Clear();
		ResizeCanvas(new RectOffset(-xMin, xMax-(w-1), yMax-(h-1), -yMin));
	}

	public void FrameDupe() {
		FrameAdd(true);
	}

	public void FrameAddButton() {
		FrameAdd();
	}

	public void FrameAdd(bool duplicate = false, bool undoAction = false) {
		Texture2D t = NewTex();
		frames.Insert(currentFrame+1, t);
		frameIndices.Insert(currentFrame+1, new int[w*h]);
		currentFrame++;
		quad.material.mainTexture = canvas;
		for(int i = 0; i < paletteIndices.Length; i++) {
			frameIndices[currentFrame][i] = duplicate ? frameIndices[prevFrame][i] : bgColor;
		}
		if(!undoAction) {
			UndoStep u = new UndoStep();
			if(duplicate) {
				u.AddFrame(currentFrame, paletteIndices, bgColor);
			}
			else {
				u.AddFrame(currentFrame);
			}
			UndoStack.Push(u);
			RedoStack.Clear();
		}
	}

	public void FrameRemove(bool undoAction = false) {
		if(frames.Count == 1) {
			return;
		}
		if(!undoAction) {
			UndoStep u = new UndoStep();
			u.RemoveFrame(currentFrame, frameIndices[currentFrame], bgColor);
			UndoStack.Push(u);
			RedoStack.Clear();
		}
		frameIndices.RemoveAt(currentFrame);
		frames.RemoveAt(currentFrame);
		quad.material.mainTexture = canvas;
	}

	//HACK
	bool menu = false;
	string[] files;

	// Update is called once per frame
	void Update () {
		/*
		if(Input.GetKeyDown(KeyCode.P)) {
			int[] tempPal = SortedPalette();
			Color32[] newPal = new Color32[tempPal.Length];
			foreach(int i in tempPal) {
				//Debug.Log(i + " : " + palette[i]);
				newPal[i] = palette[tempPal[i]];
				paletteEntries[i].image.color = newPal[i];
			}
			newPal.CopyTo(palette, 0);
			UpdatePalette();
		}
		*/
		//TODO: begin preview test block
		if(Input.GetKeyDown(KeyCode.N)) {
			UndoStack.Clear();
			RedoStack.Clear();
			New(w, h);
		}
		if(Input.GetKeyDown(KeyCode.Menu) || Input.GetKeyDown(KeyCode.KeypadMultiply)) {
			menu = !menu;
			files = System.IO.Directory.GetFiles(Application.persistentDataPath, "*.pxc");
		}
		if(menu) {
			return;
		}
		if(Input.GetKeyDown(KeyCode.KeypadPlus)) {
			FrameAdd();
		}
		if(Input.GetKeyDown(KeyCode.KeypadMinus)) {
			FrameRemove();
		}
		if(Input.GetKeyDown(KeyCode.LeftArrow)) {
			currentFrame--;
			quad.material.mainTexture = canvas;
			preview.material.mainTexture = canvas;
		}
		if(Input.GetKeyDown(KeyCode.RightArrow)) {
			currentFrame++;
			quad.material.mainTexture = canvas;
			preview.material.mainTexture = canvas;
		}
		preview.texture = frames[(int)(Time.time * previewFramerate) % frames.Count];
		if(Input.GetKeyDown(KeyCode.Escape)) {
			//Application.Quit();
			Undo();
		}
		//end preview test block
		
		grid.material.color = new Color(1,1,1, 1 - cam.orthographicSize/Mathf.Clamp(Mathf.Max(w, h), 0, 32));
		
		if(overlay && overlay.enabled) {
			return;
		}
		if(isDirty) {
			Color32[] cols = new Color32[paletteIndices.Length];
			for(int i = 0; i < cols.Length; i++) {
				//cols[i] = palette[paletteIndices[i] + buffer[i]];
				try {
					cols[i] = buffer[i] < 0 ? palette[paletteIndices[i]] : palette[buffer[i]];
				}
				catch {
					Debug.Log("Buffer: " + i + " - Palette: " + paletteIndices[i]);
				}
			}
			Color32[] onionCols = new Color32[paletteIndices.Length];
			if(showOnionSkin) {
				for(int i = 0; i < onionCols.Length; i++) {
					onionCols[i] = Color.clear;
					for(int onionStep = onionSteps; onionStep > 0; onionStep--) {
						if(frames.Count > onionStep*2-1) {
							if(frameIndices[FramePlus(-onionStep)][i] != bgColor) {
								//onionCols[i] = Color.Lerp(onionCols[i], palette[frameIndices[FramePlus(-onionStep)][i]] + Color.red, 0.25f-(0.25f*(float)onionStep/(onionSteps+1)));
								onionCols[i] = Color.Lerp(onionCols[i], palette[frameIndices[FramePlus(-onionStep)][i]] + Color.red, 1f / (2 << onionStep));
							}
							if(frameIndices[FramePlus(onionStep)][i] != bgColor) {
								//onionCols[i] = Color.Lerp(onionCols[i], palette[frameIndices[FramePlus(onionStep)][i]] + Color.green, 0.25f-(0.25f*(float)onionStep/(onionSteps+1)));
								onionCols[i] = Color.Lerp(onionCols[i], palette[frameIndices[FramePlus(onionStep)][i]] + Color.green, 1f / (2 << onionStep));
							}
						}
					}
				}
			}
			foreach(int i in selection) {
				onionCols[i] = Color32.Lerp(onionCols[i], cols[i].Luminance() > 127 ? Color.black : Color.white, 0.5f);
			}
			foreach(int i in tempSelection) {
				onionCols[i] = Color32.Lerp(onionCols[i], cols[i].Luminance() > 127 ? Color.black : Color.white, 0.5f);
			}
			onionSkinTex.SetPixels32(onionCols);
			onionSkinTex.Apply();
			canvas.SetPixels32(cols);
			canvas.Apply();
			SetDirty(false);
		}

		cam.orthographicSize += (targetZoom - cam.orthographicSize) * Time.deltaTime * 10;

		if(!(Input.mousePosition.y > Screen.height * (512f/1920) && Input.mousePosition.y < Screen.height * (1600f/1920))) {
			return;
		}

		if(Input.GetMouseButtonDown(0)) {
			sortedPalette = SortedPalette();
			Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
			int x = Mathf.FloorToInt(p.x);
			int y = Mathf.FloorToInt(p.y);
			//canvas.SetPixel(x, y, Color.black);
			//canvas.Apply();
			if(!(lastX == x && lastY == y) && (x >= 0 && x < canvas.width && y >= 0 && y < canvas.height)) {
				firstTouch = true;
				//UndoStack.Push(new Action());
				//ClearBuffer();
			}
		}

		if(Input.GetMouseButtonDown(1)) {
			ClearBuffer();
		}

		if(Input.GetMouseButtonDown(1)) {
			if(firstTouch) {
				//Undo();
				if(UndoStack.Count > 0 && UndoStack.Peek().isEmpty) {
					UndoStack.Pop();
				}
				ClearBuffer();
				firstTouch = false;
			}
		}

		if(Input.touchCount == 2) {
			if(firstTouch) {
				//Undo();
				if(UndoStack.Count > 0 && UndoStack.Peek().isEmpty) {
					UndoStack.Pop();
				}
				ClearBuffer();
				firstTouch = false;
			}
			Touch t0 = Input.touches[0];
			Touch t1 = Input.touches[1];
			if(t1.phase == TouchPhase.Began) {
				dragOrigin = cam.ScreenToWorldPoint((t0.position + t1.position) / 2);
			}
			float d0 = (t0.position - t1.position).magnitude;
			float d1 = ((t0.position + t0.deltaPosition) - (t1.position + t1.deltaPosition)).magnitude;

			targetZoom -= (d1 - d0)*0.01f * cam.orthographicSize;
			targetZoom = Mathf.Clamp(targetZoom, 8, Mathf.Max(w, h) * 2);
			cam.orthographicSize += (targetZoom - cam.orthographicSize) * Time.deltaTime * 10;
			//cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, 8, Mathf.Max(w, h) * 2);

			Vector3 p = cam.ScreenToWorldPoint((t0.position + t1.position) / 2);
			cam.transform.position  += dragOrigin - p;

			return;
		}

		if(Input.GetAxis("Mouse ScrollWheel") != 0) {
			targetZoom -= Input.GetAxis("Mouse ScrollWheel") * cam.orthographicSize;
			targetZoom = Mathf.Clamp(targetZoom, 8, Mathf.Max(w, h) * 2);
			//cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, 8, Mathf.Max(w, h) * 2);
			return;
		}

		if(Input.GetMouseButtonUp(0)) {
			firstTouch = false;
		}
		
		//targetZoom -= Input.GetAxis("Mouse ScrollWheel") * 10;
		//cam.orthographicSize += (targetZoom - cam.orthographicSize) * Time.deltaTime * 10;
		
		//TODO: consider drawing to buffer (visible) before writing, instead of drawing directly...
		//		this would be a good way to cancel stray draw when doing two-finger gestures
		if(Input.GetMouseButtonDown(2)) {
			dragOrigin = cam.ScreenToWorldPoint(Input.mousePosition);
		}
		if(Input.GetMouseButton(2)) {
			Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
			cam.transform.position += dragOrigin - p;
		}

		if(paintMode == paintModes.Select) {
			if(Input.GetMouseButtonDown(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				lastX = Mathf.FloorToInt(p.x);
				lastY = Mathf.FloorToInt(p.y);
			}
			if(Input.GetMouseButton(0) && firstTouch) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				//canvas.SetPixel(x, y, Color.black);
				//canvas.Apply();
				if(x >= 0 && x < canvas.width && y >= 0 && y < canvas.height) {
					Select(lastX, lastY, x, y);
				}
			}
			if(Input.GetMouseButtonUp(0)) {
				foreach(int i in tempSelection) {
					if(selection.Contains(i)) {
						continue;
					}
					selection.Add(i);
				}
				tempSelection.Clear();
				SetDirty(true);
				/*
				if(selection.Count == 1) {
					selection.Clear();
					SetDirty(true);
				}
				*/
			}
		}
		
		if(paintMode == paintModes.Line) {
			if(Input.GetMouseButtonDown(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = lastX = Mathf.FloorToInt(p.x);
				int y = lastY = Mathf.FloorToInt(p.y);
				if(x >= 0 && x < canvas.width && y >= 0 && y < canvas.height) {
					UndoStack.Push(new UndoStep());
					RedoStack.Clear();
				}
			}
			if(Input.GetMouseButton(0) && firstTouch) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				//canvas.SetPixel(x, y, Color.black);
				//canvas.Apply();
				if(x >= 0 && x < canvas.width && y >= 0 && y < canvas.height) {
					//Select(lastX, lastY, x, y);
					Line(lastX, lastY, x, y);
				}
			}
			if(Input.GetMouseButtonUp(0)) {
				FlushBuffer();
			}
		}
		
		if(paintMode == paintModes.Brush) {
			if(Input.GetMouseButtonDown(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				lastX = x;
				lastY = y;
				//canvas.SetPixel(x, y, Color.black);
				//canvas.Apply();
				//if(!(lastX == x && lastY == y) && (x >= 0 && x < canvas.width && y >= 0 && y < canvas.height)) {
				if(x >= 0 && x < canvas.width && y >= 0 && y < canvas.height) {
					UndoStack.Push(new UndoStep());
					RedoStack.Clear();
					Draw(x, y, (int)brushRadius, paletteIndex);
				}
			}
			if(Input.GetMouseButton(0) && firstTouch) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				//canvas.SetPixel(x, y, Color.black);
				//canvas.Apply();
				if(!(lastX == x && lastY == y) && (x >= 0 && x < canvas.width && y >= 0 && y < canvas.height)) {
					//TODO: modify Draw routine to use Bresenham?
					//Draw(x, y, (int)brushRadius, paletteIndex);
					Line(lastX, lastY, x, y, true);
					lastX = x;
					lastY = y;
				}
			}
			if(Input.GetMouseButtonUp(0)) {
				/*
				if(Time.time <= tempTimer + 0.1f) {
					Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
					int x = Mathf.FloorToInt(p.x);
					int y = Mathf.FloorToInt(p.y);
					//canvas.SetPixel(x, y, Color.black);
					//canvas.Apply();
					if(!(lastX == x && lastY == y) && (x >= 0 && x < canvas.width && y >= 0 && y < canvas.height)) {
						lastX = x;
						lastY = y;
						Draw(x, y, (int)brushRadius, paletteIndex);
					}
				}
				*/
				lastX = -1;
				lastY = -1;
				//if(UndoStack.Count > 0 && UndoStack.Peek().Count == 0) {
				if(UndoStack.Count > 0 && UndoStack.Peek().isEmpty) {
					//UndoStack.Pop();
				}
				FlushBuffer();
			}
		}
		if(paintMode == paintModes.Flood) {
			if(Input.GetMouseButtonDown(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				if(x < canvas.width && y >= 0 && y < canvas.height) {
					UndoStack.Push(new UndoStep());
					RedoStack.Clear();
					Flood(x, y, paletteIndex, floodTolerance);
				}
			}
			if(Input.GetMouseButtonUp(0)) {
				//if(UndoStack.Count > 0 && UndoStack.Peek().Count == 0) {
				if(UndoStack.Count > 0 && UndoStack.Peek().isEmpty) {
					//UndoStack.Pop();
				}
				FlushBuffer();
			}
		}
		if(paintMode == paintModes.Move || paintMode == paintModes.Clone) {
			if(Input.GetMouseButtonDown(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				if(x < canvas.width && y >= 0 && y < canvas.height) {
					UndoStack.Push(new UndoStep());
					RedoStack.Clear();
					ClearBuffer();
					PickupSelection(paintMode == paintModes.Clone);
					lastX = Mathf.FloorToInt(p.x);
					lastY = Mathf.FloorToInt(p.y);
					selX = lastX;
					selY = lastY;
				}
			}
			if(Input.GetMouseButton(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				//canvas.SetPixel(x, y, Color.black);
				//canvas.Apply();
				//if(!(lastX == x && lastY == y) && (x >= 0 && x < canvas.width && y >= 0 && y < canvas.height)) {
				if(x >= 0 && x < canvas.width && y >= 0 && y < canvas.height) {
					//Move(lastX-x, lastY-y);
					Move(selX-x, selY-y);
					//MoveBuffer(selX-x, selY-y);
					MoveSelection(selX-x, selY-y);
					selX = x;
					selY = y;
					//lastX = x;
					//lastY = y;
				}
			}
			if(Input.GetMouseButtonUp(0)) {
				//if(UndoStack.Count > 0 && UndoStack.Peek().Count == 0) {
				if(UndoStack.Count > 0 && UndoStack.Peek().isEmpty) {
					//UndoStack.Pop();
				}
				//FlushBuffer();
				//PickupSelection();
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				if(x >= 0 && x < canvas.width && y >= 0 && y < canvas.height) {
					DropSelection();
				}
			}
		}
		if(paintMode == paintModes.Eyedropper) {
			if(Input.GetMouseButton(0)) {
				Vector3 p = cam.ScreenToWorldPoint(Input.mousePosition);
				int x = Mathf.FloorToInt(p.x);
				int y = Mathf.FloorToInt(p.y);
				if(!(lastX == x && lastY == y) && (x >= 0 && x < canvas.width && y >= 0 && y < canvas.height)) {
					lastX = x;
					lastY = y;
					Eyedropper(x, y);
				}
			}
			if(Input.GetMouseButtonUp(0)) {
				lastX = -1;
				lastY = -1;
			}
		}

	}

	public void Line(int xCoord, int yCoord, int xEnd, int yEnd, bool continuous = false) {
		int dx = Mathf.Abs(xEnd - xCoord);
		int dy = Mathf.Abs(yEnd - yCoord);
		int sx = xCoord < xEnd ? 1 : -1;
		int sy = yCoord < yEnd ? 1 : -1;
		int x0 = xCoord;
		int y0 = yCoord;
		float err = dx - dy;
		int y = yCoord;
		if(!continuous) {
			for(int i = 0; i < buffer.Length; i++) {
				buffer[i] = -1;
			}
		}
		//for(int y = 0; y < h; y++) {
		//for(int x = x0; x <= x1; x++) {

		//buffer[yCoord * w + xCoord] = paletteIndex;
		//buffer[yEnd * w + xEnd] = paletteIndex;
		Draw(xCoord, yCoord, (int)brushRadius, paletteIndex);
		Draw(xEnd, yEnd, (int)brushRadius, paletteIndex);
		for(int i = 0; i < buffer.Length; i++) {
			if(xCoord == xEnd && yCoord == yEnd) {
				break;
			}
			float e2 = err * 2;
			if(e2 > -dy) {
				err -= dy;
				xCoord += sx;
			}
			if(e2 < dx) {
				err += dx;
				yCoord += sy;
				yCoord = Mathf.Clamp(yCoord, -h, h);
			}
			int idx = yCoord * w + xCoord;
			//buffer[idx] = paletteIndex;
			Draw(xCoord, yCoord, (int)brushRadius, paletteIndex);
			//buffer[i] = Mathf.Sqrt((x-xCoord)*(x-xCoord) + (y-yCoord)*(y-yCoord)) - Mathf.Sqrt((new Vector2(x1-x0, y1-y0)).magnitude) < 10f ? paletteIndex : bgColor;
				//buffer[i] = x >= x0 && x <= x1 && y >= y0 && y <= y1 && Mathf.Abs((y-yCoord) - (x-xCoord)*((float)(yEnd-yCoord)/(xEnd-xCoord))) <= 0.5f ? paletteIndex : bgColor;
		}
		//}
		SetDirty(true);
	}
	
	private void Eyedropper(int xCoord, int yCoord) {
		int i = yCoord * canvas.width + xCoord;
		SetPalette(paletteIndices[i]);
		//UpdateColorIndicator();
	}

	public void UpdatePalette() {
		SetDirty(true);
	}

	public void AddFrame(int index = -1) {
		/*
		Texture2D t = new Texture2D(w, h, TextureFormat.ARGB32, false);
		t.filterMode = FilterMode.Point;
		t.wrapMode = TextureWrapMode.Clamp;
		*/
		Texture2D t = NewTex();
		int[] tempIndices = new int[w*h];
		for(int i = 0; i < tempIndices.Length; i++) {
			tempIndices[i] = bgColor;
		}
		frames.Insert(index > -1 ? index : currentFrame, t);
		frameIndices.Insert(index > -1 ? index : currentFrame, tempIndices);
		currentFrame = index > -1 ? index : currentFrame;
		/*
		buffer = deltas.ToArray();
		SetDirty(true);
		*/
		quad.material.mainTexture = canvas;
		preview.material.mainTexture = canvas;
	}

	public void RemoveFrame(int index = -1) {
		frameIndices.RemoveAt(index > -1 ? index : currentFrame);
		frames.RemoveAt(index > -1 ? index : currentFrame);
		quad.material.mainTexture = canvas;
		preview.material.mainTexture = canvas;
	}
	
	private void UpdateColorIndicator() {
		Color[] palCols = new Color[colorIndicator.width*colorIndicator.height];
		for(int i = 0; i < palCols.Length; i++) {
			palCols[i] = palette[paletteIndex];
		}
		colorIndicator.SetPixels(palCols);
		colorIndicator.Apply();
	}

	public void MoveSelection(int deltaX, int deltaY) {
		for(int i = 0; i < selection.Count; i++) {
			int idx = selection[i];
			int x = idx % canvas.width;
			int y = idx / canvas.width;
			int newIdx = (int)(Mathf.Repeat(y-deltaY, canvas.height)) * canvas.width + (int)(Mathf.Repeat(x-deltaX, canvas.width));
			selection[i] = newIdx;
		}
	}

	public void PickupSelection(bool clone = false) {
		if(selection.Count == 0) {
			for(int i = 0; i < buffer.Length; i++) {
				buffer[i] = paletteIndices[i];
			}
		}
		foreach(int i in selection) {
			buffer[i] = paletteIndices[i] == bgColor ? -1 : paletteIndices[i];// - bgColor;
			if(!clone) {
				selectionBuffer[i] = paletteIndices[i];
				paletteIndices[i] = bgColor;
			}
		}
	}

	public void DropSelection() {
		/*
		foreach(int i in selection) {
			paletteIndices[i] += buffer[i];
		}
		ClearBuffer();
		*/
		FlushBuffer();
	}
	
	public void Move(int deltaX, int deltaY, bool undoOp = false) {
		//paletteIndices.CopyTo(buffer, 0);
		//ClearBuffer();
		int[] temp = new int[buffer.Length];
		//paletteIndices.CopyTo(temp, 0);
		buffer.CopyTo(temp, 0);
		for(int y = 0; y < canvas.height; y++) {
			for(int x = 0; x < canvas.width; x++) {
				int i = y * canvas.width + x;
				int newIdx = (int)(Mathf.Repeat(y+deltaY, canvas.height)) * canvas.width + (int)(Mathf.Repeat(x+deltaX, canvas.width));
				//paletteIndices[i] = oldIndices[oldIdx];
				buffer[i] = temp[newIdx];// - paletteIndices[i];
			}
		}
		SetDirty(true);
	}

	public void Select(int xCoord, int yCoord, int xEnd, int yEnd) {
		tempSelection.Clear();
		int x = Mathf.Min(xCoord, xEnd);
		int y = Mathf.Min(yCoord, yEnd);
		int w = Mathf.Max(xCoord, xEnd);
		int h = Mathf.Max(yCoord, yEnd);
		for(int yy = y; yy <= h; yy++) {
			for(int xx = x; xx <= w; xx++) {
				int i = yy * canvas.width + xx;
				tempSelection.Add(i);
			}
		}
		SetDirty(true);
	}
	
	public void ClearBuffer() {
		for(int i = 0; i < buffer.Length; i++) {
			buffer[i] = -1;
			selectionBuffer[i] = -1;
		}
		SetDirty(true);
	}

	public void FlushBuffer() {
		for(int i = 0; i < buffer.Length; i++) {
			if(selectionBuffer[i] < 0) {// || buffer[i] >= 0) {
				continue;
			}
			paletteIndices[i] = selectionBuffer[i];
			buffer[i] = buffer[i] < 0 ? bgColor : buffer[i];
		}
		if(UndoStack.Count > 0) {
			if(!UndoStack.Peek().FromBuffer(buffer, paletteIndices, currentFrame)) {
			//if(!undo) {
				UndoStack.Pop();
			}
		}
		for(int i = 0; i < buffer.Length; i++) {
			/*
			if(buffer[i] == 0) {
				continue;
			}
			*/
			paletteIndices[i] = buffer[i] < 0 ? paletteIndices[i] : buffer[i];	//buffer will store the delta palette index
		}
		ClearBuffer();
	}
	
	public void Draw(int xCoord, int yCoord, int radius, int palIndex, bool undoOp = false) {
		//Debug.Log(xCoord + ","  + yCoord);
		for(int dy = -radius; dy <= radius; dy++) {
			int y = yCoord+dy;
			if(y < 0 || y >= canvas.height) {
				continue;
			}
			for(int dx = -radius; dx <= radius; dx++) {
				int x = xCoord+dx;
				if(x < 0 || x >= canvas.width) {
					continue;
				}
				if(dx*dx+dy*dy < radius*radius) {
					int i = y * canvas.width + x;
					int p;
					if(antiAlias) {
						//int[] sortedPalette = SortedPalette();
						//int p0 = palIndex;
						//int p1 = paletteIndices[i] + buffer[i];
						int p0 = System.Array.IndexOf(sortedPalette, palIndex);//sortedPalette[palIndex];
						int p1 = System.Array.IndexOf(sortedPalette, buffer[i] < 0 ? paletteIndices[i] : buffer[i]);// + buffer[i]);//sortedPalette[paletteIndices[i] + buffer[i]];
						p = Mathf.RoundToInt(Mathf.Lerp(p0, p1, Mathf.Clamp01((float)(dx*dx+dy*dy)/(radius*radius*radius))));
						//p = Mathf.RoundToInt(Mathf.Lerp(p0, p1, Mathf.Clamp01(Mathf.Sqrt((float)(dx*dx+dy*dy))/(radius*radius))));
						p = sortedPalette[p];
					}
					else {
						//p = (Mathf.Clamp01((float)(dy*dy+dx*dx)/(radius*radius))) < 0.71f ? paletteIndex : paletteIndices[i];
						p = (new Vector2(dx,dy)).magnitude / radius < 0.666f ? palIndex : buffer[i];
					}
					//int p = Mathf.RoundToInt(Mathf.Lerp(p0, p1, 1-(Mathf.Sqrt(dy*dy+dx*dx)/radius)));
					//cols[i] = Color.Lerp(cols[i], col, 1-(float)(dy*dy+dx*dx)/(radius*radius));
					if(!undoOp) {
						//UndoStack.Peek().Add(eOperations.DRAW, x, y, paletteIndices[i], p);
					}
					//paletteIndices[i] = p;
					//Debug.Log(i);
					buffer[i] = p;// - paletteIndices[i];
				}
			}
		}
		SetDirty(true);
	}
	
	public void Flood(int xCoord, int yCoord, int palIndex, float tolerance, bool undoOp = false) {
		int colIndex = paletteIndices[yCoord * canvas.width + xCoord];
		List<int> processed = new List<int>();
		FloodStep(xCoord, yCoord, colIndex, palIndex, tolerance, ref processed, undoOp);
	}
	
	private void FloodStep(int xCoord, int yCoord, int colIndex, int palIndex, float tolerance, ref List<int> processed, bool undoOp = false) {
		if(xCoord < 0 || xCoord > canvas.width-1 || yCoord < 0 || yCoord > canvas.height-1) {
			return;
		}
		int idx = yCoord * canvas.width + xCoord;
		if(processed.Contains(idx)) {
			return;
		}
		//if(cols[idx] != col) {
		//if(Mathf.Abs(paletteIndices[idx] - colIndex) > (int)(floodTolerance * palette.Length)) {
		if(Mathf.Abs(paletteIndices[idx] - colIndex) > tolerance) {
		//if(paletteIndices[idx] != colIndex) {
			return;
		}
		//paletteIndices[idx] = palIndex;
		buffer[idx] = palIndex;// - colIndex;
		processed.Add(idx);
		SetDirty(true);
		if(!undoOp) {
			//UndoStack.Peek().Add(eOperations.DRAW, xCoord, yCoord, colIndex, palIndex);
		}
		FloodStep(xCoord+1, yCoord, colIndex, palIndex, tolerance, ref processed);
		FloodStep(xCoord-1, yCoord, colIndex, palIndex, tolerance, ref processed);
		FloodStep(xCoord, yCoord+1, colIndex, palIndex, tolerance, ref processed);
		FloodStep(xCoord, yCoord-1, colIndex, palIndex, tolerance, ref processed);
	}

	private void Test() {
		for(int x = 0; x < canvas.width; x++) {
			int y = Mathf.FloorToInt((Mathf.Sin(((float)x / canvas.width) * Mathf.PI * 2) *0.5f+0.5f) * canvas.height);
			canvas.SetPixel(x, y, Color.black);
			canvas.SetPixel(x, y-1, Color.black);
			canvas.SetPixel(x, y+1, Color.black);
		}
		canvas.Apply();
	}
	
	public void SetMode(int mode) {
		//should probably have a current mode property to check against
		//for move/clone/etc, can check if we're switching away, then can check undo state
		//TODO: have undo check for buffer state... if buffer full, we're midway through storing an undo, perhaps? something like that... for move
		//alternately, move/clone should be constantly overwriting a single undo step

		switch(mode) {
		/*	//generally: start an undo here, then check the undo when switching away from move/clone/etc
		case (int)paintModes.Move:
			DropSelection();
			ClearBuffer();
			PickupSelection(false);
			break;
		*/
		case (int)paintModes.Brush:
		case (int)paintModes.Line:
			brushRadiusSlider.gameObject.SetActive(true);
			floodToleranceSlider.gameObject.SetActive(false);
			selection.Clear();
			SetDirty(true);
			break;
		case (int)paintModes.Flood:
			if(selection.Count > 0) {
				UndoStack.Push(new UndoStep());
				RedoStack.Clear();
				foreach(int i in selection) {
					buffer[i] = paletteIndex;
				}
				FlushBuffer();
				return;
			}
			brushRadiusSlider.gameObject.SetActive(false);
			floodToleranceSlider.gameObject.SetActive(true);
			break;
		case (int)paintModes.MirrorH:
			PickupSelection();
			UndoStack.Push(new UndoStep());
			RedoStack.Clear();
			if(selection.Count > 0) {
				int xMin = w;
				int yMin = h;
				int xMax = 0;
				int yMax = 0;
				foreach(int i in selection) {
					int x = i % w;
					int y = i / w;
					if(x < xMin) xMin = x;
					if(x > xMax) xMax = x;
					if(y < yMin) yMin = y;
					if(y > yMax) yMax = y;
				}
				//Debug.Log(xMin + " : " + xMax + " : " + yMin + " : " + yMax);
				float m = (float)(xMax - xMin)/2 + xMin;
				int[] tempBuffer = new int[buffer.Length];
				buffer.CopyTo(tempBuffer, 0);
				//foreach(int i in selection) {
				for(int s = 0; s < selection.Count; s++) {
					int i = selection[s];
					int x = i % w;
					int y = i / w;
					//Debug.Log(x + " : " + (-(x-m)+m));
					x = (int)(-(x - m) + m);
					int newIdx = y * w + x;
					buffer[newIdx] = tempBuffer[i];
					buffer[i] = tempBuffer[newIdx];
					selection[s] = newIdx;
				}
			}
			else {
				int[] tempBuffer = new int[buffer.Length];
				buffer.CopyTo(tempBuffer, 0);
				float m = (float)(w-1) / 2;
				for(int y = 0; y < h; y++) {
					for(int x = 0; x < w; x++) {
						int i = y * w + x;
						int newIdx = y * w + (int)(-(x-m)+m);
						buffer[newIdx] = tempBuffer[i];
					}
				}
			}
			DropSelection();
			return;
		case (int)paintModes.Select:
			if(paintMode == paintModes.Select) {	//tapping this while already in select mode clears selection
				selection.Clear();
				SetDirty(true);
			}
			break;
		default:
			brushRadiusSlider.gameObject.SetActive(false);
			floodToleranceSlider.gameObject.SetActive(false);
			break;
		}
		paintMode = (paintModes)mode;
		UpdateToolButtons();
	}
	
	private void UpdateToolButtons() {
		for(int i = 0; i < toolButtons.Length; i++) {
			if(i == (int)paintMode) {
				toolButtons[i].image.color = Color.white;
			}
			else {
				toolButtons[i].image.color = Color.white * 0.5f;
			}
		}
	}

	public void New(Vector2 size) {
		New((int)size.x, (int)size.y);
	}

	public void New(int w, int h) {
		this.w = w;
		this.h = h;
		frames.Clear();
		frameIndices.Clear();
		canvas = new Texture2D(w, h, TextureFormat.ARGB32, false);
		canvas.filterMode = FilterMode.Point;
		canvas.wrapMode = TextureWrapMode.Clamp;
		onionSkinTex = new Texture2D(w, h, TextureFormat.ARGB32, false);
		onionSkinTex.filterMode = FilterMode.Point;
		onionSkinTex.wrapMode = TextureWrapMode.Clamp;
		onionSkin.material.mainTexture = onionSkinTex;
		grid.material.mainTextureScale = new Vector2(w, h);
		paletteIndices = new int[canvas.width*canvas.height];
		buffer = new int[w*h];
		selectionBuffer = new int[w*h];
		Color[] cols = new Color[w*h];
		for(int i = 0; i < paletteIndices.Length; i++) {
			paletteIndices[i] = bgColor;
			cols[i] = palette[paletteIndices[i]];
			buffer[i] = -1;
			selectionBuffer[i] = -1;
		}
		//canvas.SetPixels(cols);
		//canvas.Apply();
		quad.material.mainTexture = canvas;
		SetDirty(true);
		//preview.material.mainTexture = canvas;
		preview.texture = canvas;
		quad.transform.parent.localScale = new Vector3(w, h, 1);
		//grid.material.mainTextureOffset = new Vector2(-0.5f / grid.transform.parent.localScale.x, 0.5f / grid.transform.parent.localScale.x);
		float mul = Mathf.Floor(128 / Mathf.Max(w,h));
		while(mul > 2 && Mathf.Sqrt(mul) % 2 != 0) {
			mul--;
		}
		preview.rectTransform.anchoredPosition = Vector2.up * h * mul;
		preview.rectTransform.offsetMax = new Vector2(w, h) * mul;
		preview.rectTransform.offsetMin = -new Vector2(w, h) * mul;
		//cam.orthographicSize = (Mathf.Min(w, h) * 0.5f) / cam.aspect;
		targetZoom = cam.orthographicSize = Mathf.Min(w, h);
		cam.transform.position = new Vector3(w * 0.5f, h * 0.4f, -10);
		//cam.orthographicSize = cam.pixelHeight;

	}

	//HACK
	Vector2 scrollPos;
	//(Input.mousePosition.y > Screen.height * (512f/1920) && Input.mousePosition.y < Screen.height * (1600f/1920)
	void OnGUI() {
		GUILayout.Label("Undo Stack: " + UndoStack.Count + " : Redo Stack: " + RedoStack.Count);
		GUILayout.Label("Frame Count: " + frames.Count + " :  Current Frame: " + currentFrame);
		//GUILayout.Label(prevFrame + " : " + currentFrame + " : " + nextFrame);
		//GUILayout.Label((Input.mousePosition.y > Screen.height * (512f/1920) && Input.mousePosition.y < Screen.height * (1600f/1920)).ToString());
		if(menu) {
			GUIStyle b = new GUIStyle(GUI.skin.button);
			b.fontSize = Screen.height / 16;
			b.padding = new RectOffset(8,8,8,8);
			GUI.Box(new Rect(0,0,Screen.width,Screen.height), "");
			GUILayout.BeginArea(new Rect(0, Screen.height * (320f/1920), Screen.width, 1088));
			scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Width(Screen.width), GUILayout.Height(Screen.height * (1088f/1920)));
			foreach(string f in files) {
				string filename = System.Text.RegularExpressions.Regex.Replace(f, ".*[/\\\\]", "");
				filename = filename.Replace(".pxc", "");
				if(GUILayout.Button(filename, b)) {
					filenameInput.value = filename;
					Load();
					menu = false;
				}
			}
			GUILayout.EndScrollView();
			GUILayout.EndArea();
		}
	}
}

[System.Serializable]
public class CanvasSize {
	public int left, right, top, bottom;
	public CanvasSize(int left, int right, int top, int bottom) {
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;
	}

	public static implicit operator RectOffset(CanvasSize c) {
		return new RectOffset(c.left, c.right, c.top, c.bottom);
	}

	public int horizontal {
		get {
			return left + right;
		}
	}
	public int vertical {
		get {
			return top + bottom;
		}
	}
}

[System.Serializable]
public class UndoStep {
	/*
	 * TODO:
	 * palette changes (color adjustments)
	 * canvas resize
	 */
	public int frameIndex;
	public int frameDelta;
	public List<int> indices = new List<int>();
	public List<int> paletteDeltas = new List<int>();
	public CanvasSize canvasResize;
	public List<int[]> frames = new List<int[]>();
	public bool isEmpty {
		get {
			return indices.Count == 0;
		}
	}

	public void ResizeCanvas(RectOffset offset, List<int[]> framePixels) {
		canvasResize = new CanvasSize(offset.left, offset.right, offset.top, offset.bottom);
		frames.Clear();
		foreach(int[] i in framePixels) {
			frames.Add(i);
		}
	}

	public void AddFrame(int frame, int[] paletteIndices, int bgColor) {
		for(int i = 0; i < paletteIndices.Length; i++) {
			indices.Add(i);
			paletteDeltas.Add(paletteIndices[i] - bgColor);
		}
		AddFrame(frame);
	}

	public void AddFrame(int frame) {
		frameIndex = frame;
		frameDelta = 1;
	}

	public void RemoveFrame(int frame, int[] paletteIndices, int bgColor) {
		for(int i = 0; i < paletteIndices.Length; i++) {
			indices.Add(i);
			paletteDeltas.Add(bgColor - paletteIndices[i]);
		}
		frameIndex = frame;
		frameDelta = -1;
	}

	public bool FromBuffer(int[] buffer, int[] paletteIndices, int frame, int fDelta = 0) {	//fDelta: -1 deleted frame, 1 added, 0 no change
		for(int i = 0; i < buffer.Length; i++) {
			if(buffer[i] != -1 && buffer[i] - paletteIndices[i] != 0) {
				indices.Add(i);
				paletteDeltas.Add(buffer[i] - paletteIndices[i]);
			}
		}
		frameIndex = frame;
		frameDelta = fDelta;
		return paletteDeltas.Count > 0 || frameDelta != 0;
	}

	public void Undo(PixelEdit p) {
		if(canvasResize != null) {
			//p.ResizeCanvas(new RectOffset(-canvasResize.left, -canvasResize.right, -canvasResize.top, -canvasResize.bottom), true);
			p.New(p.w - canvasResize.horizontal, p.h - canvasResize.vertical);
			p.frames.Clear();
			p.frameIndices.Clear();
			p.ClearBuffer();
			for(int i = 0; i < frames.Count; i++) {
				p.frames.Add(p.NewTex());
				p.frameIndices.Add(frames[i]);
				p.WriteFrame(i);
			}
			p.currentFrame = p.currentFrame;
			return;
		}
		if(frameDelta > 0) {
			//p.RemoveFrame(frameIndex);
			p.currentFrame = frameIndex;
			p.FrameRemove(true);
			p.SetDirty(true);
			return;
		}
		if(frameDelta < 0) {
			//p.AddFrame(frameIndex);
			p.currentFrame = frameIndex;
			p.FrameAdd(false, true);
		}
		p.currentFrame = frameIndex;
		for(int i = 0; i < indices.Count; i++) {
			p.frameIndices[frameIndex][indices[i]] -= paletteDeltas[i];
		}
		p.SetDirty(true);
	}

	public void Redo(PixelEdit p) {
		if(canvasResize != null) {
			p.ResizeCanvas(canvasResize, true);
			return;
		}
		if(frameDelta < 0) {
			//p.RemoveFrame(frameIndex);
			p.currentFrame = frameIndex;
			p.FrameRemove(true);
			p.SetDirty(true);
			return;
		}
		if(frameDelta > 0) {
			//p.AddFrame(frameIndex);
			p.currentFrame = frameIndex;
			p.FrameAdd(false, true);
		}
		p.currentFrame = frameIndex;
		for(int i = 0; i < indices.Count; i++) {
			p.frameIndices[frameIndex][indices[i]] += paletteDeltas[i];
		}
		p.SetDirty(true);
	}
}
